<?php
require_once (__DIR__ . '/vendor/autoload.php');
use test\core\Application;

$app = new Application(dirname(__DIR__));
$app->router->post('/test/index.php/insertUser', [test\controllers\user::class, 'addUser']);
$app->router->post('/test/index.php/getUser', [test\controllers\user::class, 'getUser']);
$app->router->get('/test/index.php/getMenu', [test\controllers\menu::class, 'getMenu']);
$app->router->post('/test/index.php/addCart', [test\controllers\cart::class, 'addToCart']);
$app->router->put('/test/index.php/updateUser', [test\controllers\user::class, 'updateUser']);
$app->router->post('/test/index.php/createOrder', [test\controllers\orders::class, 'order']);
$app->router->delete('/test/index.php/deleteUser', [test\controllers\user::class, 'deleteUser']);

$app->run();
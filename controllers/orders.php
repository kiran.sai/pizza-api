<?php

namespace test\controllers;
use test\libraries\payment_gateway;
use test\controllers\authenticate;
use test\models\order_model;

class Orders
{
    public $payment_gateway;
    public $authenticate;
    public $order;

    public function __construct() {
        $this->payment_gateway = new Payment_gateway();
        $this->authenticate = new Authenticate();
        $this->order = new Order_model();
    }

    public function order() {
        $orderData = [];
        $paymentData = [];
        $paymentDataAdd = [];

        $post = json_decode(file_get_contents("php://input"), true);

        if (!isset($_GET['token']) || empty($_GET['token'])) {
            $res['status'] = 0;
            $res['status_code'] = 401;
            $res['message'] = "Need token!!";

            exit(json_encode($res));
        }

        $authUser = $this->authenticate->authenticateUser($_GET['token']);

        if ($authUser['status'] == 1) {
            $orderData['user_id'] = $authUser['data']->id;
            $orderData['price'] = $post['total_amount'];

            $addOrder = $this->order->addOrder($orderData);

            if ($addOrder) {
                $this->order->addOrderProducts($post['menu'], $addOrder);
                $paymentData = $post['card_details'];
                $paymentData['amount'] = $post['total_amount'];
                $charge = json_decode(json_encode($this->payment_gateway->payment($paymentData)), true);


                if (is_array($charge)) {
                    $paymentDataAdd['status'] = $charge['status'];
                    $paymentDataAdd['amount'] = $charge['amount'];
                    $paymentDataAdd['user'] = $orderData['user_id'];
                    $paymentDataAdd['currency'] = $charge['currency'];
                    $paymentDataAdd['order_id'] = $addOrder;

                    if ($paymentDataAdd['status'] == "succeeded") {
                        $res['status'] = 1;
                        $res['status_code'] = 200;
                        $res['message'] = 'Payment successful and order placed!!';
                    } else {
                        $paymentDataAdd['failure_message'] = $charge['failure_message'];
                        $res['status'] = 0;
                        $res['status_code'] = 402;
                        $res['message'] = $charge['failure_message'];
                    }

                    if ($this->order->addPaymentRecord($paymentDataAdd)) {
                        $this->order->updataOrderStatus(['status' => 'SUCCESS'], ['id' => $addOrder]);

                        exit(json_encode($res));
                    }

                }

            }

            exit(json_encode($res));
        }
    }
}
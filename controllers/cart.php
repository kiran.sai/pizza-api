<?php


namespace test\controllers;

use test\models\Cart_model;

require_once ('authenticate.php');

class Cart
{
    public $cart;
    public $authenticate;

    public function __construct() {
        $this->authenticate = new Authenticate();
        $this->cart = new Cart_model();
    }
    public function addToCart() {
        $res = [];
        $authUser = $this->authenticate->authenticateUser($_GET['token']);
        $post = json_decode(file_get_contents("php://input"), true);


        if ($authUser['status']) {
            $menu = $this->cart->addCart($post);
            if ($menu) {
                $res['status'] = 1;
                $res['status_code'] = 200;
                $res['message'] = "Added to cart successfully|";
                exit(json_encode($res));
            }

            $res['status'] = 0;
            $res['status_code'] = 400;
            $res['message'] = 'please check columns';

            exit(json_encode($res));
        }

        $res['status'] = 0;
        $res['message'] = $authUser['message'];
        exit(json_encode($res));
    }
}
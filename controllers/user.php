<?php

namespace test\controllers;
use test\models\user_model;
use \Firebase\JWT\JWT;

class User {
    private $user;

    public function __construct() {
        $this->user = new User_model();
    }

    public function addUser() {
        $res = [];
        $res['status'] = false;
        $post = json_decode(file_get_contents("php://input"), true);
        $inp_ele = ['name', 'email', 'address', 'password'];

        $validateParams = $this->validateUserParams($post, $inp_ele);

        if (!$validateParams['status']) {
            echo json_encode($validateParams); exit;
        }

        $post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        $post['status'] = 1;
        $insData = $this->user->insert_user($post);
        if ($insData) {
            $res['status'] = true;
            $res['status_code'] = 200;
            $res['message'] = 'User created successfully';

            echo json_encode($res); exit;
        }

        echo json_encode($res); exit;
    }

    public function updateUser() {
        $res = [];
        $res['status'] = false;
        $post = json_decode(file_get_contents("php://input"), true);
        $id = $post['id'];
        $userData = $this->user->getUser(['*'], ['id' => $id]);

        if (count($userData)) {
            unset($post['id']);
            if ($this->user->updateUser($post, ['id' => $id])) {
                $res['status'] = true;
                $res['status_code'] = 200;
                $res['message'] = "Updated Successfully!";
            } else {
                $res['status'] = false;
                $res['message'] = "Not Updated|";
            }
        } else {
            $res['status_code'] = 400;
            $res['message'] = "User not found|";
        }

        echo json_encode($res); exit;
    }

    public function getUser() {
        $data = json_decode(file_get_contents("php://input"));
        $email = $data->email;
        $password = $data->password;

        $row = $this->user->getUser(['*'], ['email' => $email, 'status' => 1])[0];

        if (count($row)) {
            $id = $row['id'];
            $name = $row['name'];
            $address = $row['address'];
            $password2 = $row['password'];

            if (password_verify($password, $password2)) {
                $secret_key = "test";
                $issuer_claim = "THE_ISSUER"; // this can be the servername
                $audience_claim = "THE_AUDIENCE";
                $issuedat_claim = time(); // issued at
                $notbefore_claim = $issuedat_claim + 10; //not before in seconds
                $expire_claim = $issuedat_claim + 30000; // expire time in seconds
                $token = array(
                    "iss" => $issuer_claim,
                    "aud" => $audience_claim,
                    "iat" => $issuedat_claim,
                    "nbf" => $notbefore_claim,
                    "exp" => $expire_claim,
                    "data" => array(
                        "id" => $id,
                        "name" => $name,
                        "address" => $address,
                        "email" => $email
                    ));

                http_response_code(200);

                $jwt = JWT::encode($token, $secret_key);
                echo json_encode(
                    array(
                        "status" => 1,
                        "message" => "Successful login.",
                        "jwt" => $jwt,
                        "email" => $email,
                        "expireAt" => $expire_claim
                    )); exit;
            } else {
                echo json_encode(array('status' => 0, 'status_code' => 400, "message" => "Login failed.", "password" => $password)); exit();
            }
        } else {
            echo json_encode(array('status' => 0, 'status_code' => 400, "message" => "Login failed.", "password" => $email)); exit;
        }
    }

    private function validateUserParams($post, $inp_ele): array
    {
        $res['status'] = 1;
        if (!is_array($post) || count($post) == 0) {
            $res['status'] = 0;
            $res['status_code'] = 400;
            $res['message'] = 'No data';
            return ($res);
        }

        foreach ($inp_ele as $key => $value) {
            if (!in_array($value, array_keys($post)) || empty($post[$value])) {
                $res['status'] = 0;
                $res['status_code'] = 400;
                $res['message'] = "$value required";
                return ($res);
            }
        }
        return  $res;
    }

    public function deleteUser() {
        $res['status'] = 1;

        $post = json_decode(file_get_contents("php://input"), true);
        $id = $post['id'];
        $userData = $this->user->getUser(['*'], ['id' => $id]);

        if (count($userData)) {
            unset($post['id']);
            if ($this->user->deleteUser(['id' => $id])) {
                $res['status'] = true;
                $res['status_code'] = 200;
                $res['message'] = "Deleted Successfully!";
            } else {
                $res['status'] = false;
                $res['message'] = "Not deleted|";
            }
        } else {
            $res['status_code'] = 400;
            $res['message'] = "User not found|";
        }

        echo json_encode($res); exit;
    }
}
<?php


namespace test\controllers;
require_once ('authenticate.php');

use test\models\menu_model;

class menu
{
    public $menu;
    public $authenticate;

    public function __construct() {
        $this->authenticate = new Authenticate();
        $this->menu = new Menu_model();
    }

    public function getMenu () {
        $res = [];
        $authUser = $this->authenticate->authenticateUser($_GET['token']);

        if ($authUser['status']) {
            $menu = $this->menu->getMenu();
            $res['status'] = 1;
            $res['status_code'] = 200;
            $res['data'] = $menu;

            exit(json_encode($res));
        }

        $res['status'] = 0;
        $res['status_code'] = 400;
        $res['message'] = $authUser['message'];
        exit(json_encode($res));
    }
}
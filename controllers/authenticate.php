<?php


namespace test\controllers;

use Firebase\JWT\JWT;

class Authenticate
{
    public function authenticateUser($jwt)
    {
        if ($jwt) {
            try {
                $decoded = JWT::decode($jwt, 'test', array('HS256'));

                return array(
                    "status" => 1,
                    "message" => "Access granted",
                    "data" => $decoded->data
                );

            } catch (Exception $e) {

                http_response_code(401);
                return array(
                    "status" => 0,
                    "message" => "Access denied.",
                    "error" => $e->getMessage()
                );
            }
        }
    }
}
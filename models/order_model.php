<?php


namespace test\models;


class Order_model extends base
{
    public function addOrder($data) {
        $last_inserted_id = $this->insert($data, 'orders');

        if ($last_inserted_id) {
            return $last_inserted_id;
        }
        return false;
    }

    public function addOrderProducts($data, $order_id) {
        $orderProducts = [];
        foreach ($data as $key => $menu) {
            $orderProducts['order_id'] = $order_id;
            $orderProducts['menu_id'] = $menu;
            $this->insert($orderProducts, 'order_products');
        }
    }

    public function addPaymentRecord($paymentData)
    {
        $last_inserted_id = $this->insert($paymentData, 'payment_gateway');

        if ($last_inserted_id) {
            return $last_inserted_id;
        }
        return false;
    }

    public function updataOrderStatus(array $data, array $condition)
    {
        if ($this->UpdataData($data, 'orders', $condition)) {
            return true;
        }

        return false;
    }
}
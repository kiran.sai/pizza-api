<?php
namespace test\models;


class Cart_model extends base
{

    /**
     * Cart_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function addCart($data)
    {
        return $this->insert($data, 'cart');
    }
}
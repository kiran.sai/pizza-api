<?php
namespace test\models;
class User_model extends base {
    public function insert_user($data): bool
    {
        if ($this->insert($data, 'users')) {
            return true;
        }
        return false;
    }

    public function getUser(array $columns, array $condition)
    {
        return $this->getData($columns, 'users', $condition);
    }

    public function updateUser(array $data, array $condition)
    {
        return $this->UpdataData($data, 'users', $condition);
    }

    public function deleteUser(array $condition)
    {
        return $this->delete('users', $condition);
    }
}
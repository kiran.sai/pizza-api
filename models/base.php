<?php
namespace test\models;
use test\config\database;

class base
{
    private $conn;

    public function __construct() {
        $db = Database::getInstance();
        $this->conn = $db->connect();
    }

    public function UpdataData(array $data, string $table_name, array $condition)
    {
        $cond = "";

        if (count($condition)) {
            $condStr = $this->getCondition($condition);
            $cond = " WHERE $condStr";
        }
        $updateData = implode(',', array_map(array($this, 'getUpdateData'), array_keys($data)));

        $query = "Update $table_name SET $updateData $cond";
        try {
            $stmt = $this->conn->prepare($query);

            foreach ($data as $key => $value) {
                $stmt->bindParam(":$key", $data[$key]);
            }
            $stmt->execute();
            $count = $stmt->rowCount();

            if ($count == '0') {
                return false;
            } else {
                return true;
            }

        } catch (\PDOException $e) {
            echo $e->getMessage(); exit;
        }

    }

    /** @noinspection PhpMissingReturnTypeInspection */
    public function insert(array $ins_data, string $table_name) {

        if (!is_array($ins_data)) {
            return "Please pass valid data";
        } else {
            $columns = implode(',', array_keys($ins_data));
            $values = implode(',', array_map(array($this, 'getColumns'), array_keys($ins_data)));

            $query = "INSERT INTO $table_name ($columns) VALUES ($values)";
//            print_r($this->conn); exit;

            $statement = $this->conn->prepare($query);

            $statement->execute(($ins_data));

            if ($this->conn->lastInsertId()) {
                return $this->conn->lastInsertId();
            }
            return false;
        }
    }

    function getData (array $columns, string $table_name, array $condition) {
        if (count($columns) == 1 && $columns[0] == '*') {
            $columnStr = '*';
        } else {
            $columnStr = implode($columns);
        }
        $cond = "";

        if (count($condition)) {
            $condStr = $this->getCondition($condition);
            $cond = " WHERE $condStr";
        }

        $query = "SELECT $columnStr FROM " . $table_name . "$cond";
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $email);
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num > 0) {
            return $stmt->fetchAll();
        } else {
            return false;
        }
    }

    function getColumns($str): string
    {
        return sprintf(":%s", trim($str));
    }

    function getCondition($condition): string
    {
        $condArr = [];
        foreach ($condition as $key => $value) {
            $condArr[] = "$key = '$value'";
        }
        return implode(" and ", $condArr);
    }

    function getUpdateData($str): string
    {
        return sprintf("$str=:%s", trim($str));
    }

    public function delete(string $table_name, array $condition)
    {
        $cond = "";

        if (count($condition)) {
            $condStr = $this->getCondition($condition);
            $cond = " WHERE $condStr";
        }

        $step=$this->conn->prepare("DELETE FROM $table_name $cond");
        if ($step->execute()) {
            return true;
        }

        return false;
    }
}
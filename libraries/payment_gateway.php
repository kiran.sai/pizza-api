<?php

namespace test\libraries;

class Payment_gateway
{
    public function payment($card_data) {
        \Stripe\Stripe::setApiKey("sk_test_51J3FAfSDUS11Pz86FvVo7YoVl0K2sX6pZtnNKwsXX1ThAQb12Y9wdhK8t7B1CUh6k3Y2K3o6URqgxDb3q9nAmmxt000t4M7aPd");

        $token = \Stripe\Token::create([
            "card" => array(
                "number" => $card_data['number'],
                "exp_month" => $card_data['exp_month'],
                "exp_year" => $card_data['exp_year'],
                "cvc" => $card_data['cvc']
            )
        ]);

// Charge creation
        try {
            $charge = \Stripe\Charge::create([
                "amount" => $card_data['amount'],
                "currency" => "inr",
                "source" => $token->id,
                "description" => "Testing"
            ]);

            return $charge;
        } catch(Exception $e) {
            echo $e->getMessage(); exit;
        }
    }
}
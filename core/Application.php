<?php
namespace test\core;


class Application
{
    public static $ROOT_STRING;
    public static $app;
    public $router;
    public $request;
    public $response;

    public function __construct($rootPath)
    {
        self::$ROOT_STRING = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
    }

    public function run() {
        $this->router->resolve();
    }
}
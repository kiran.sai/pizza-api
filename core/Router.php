<?php

namespace test\core;

class Router
{
    protected $routes = [];
    public $request;
    public $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }
    public function get($path, $callback) {
        $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback) {
        $this->routes['post'][$path] = $callback;
    }

    public function put($path, $callback) {
        $this->routes['put'][$path] = $callback;
    }

    public function resolve()
    {
        $path = $this->request->getPath();
        $method = $this->request->getMethod();
        $callback = $this->routes[$method][$path] ?? false;

        if ($callback == false) {
            $this->response->setStatusCode(404);
            echo "Not found"; exit;
        }

        if (is_array($callback)) {
            $callback[0] = new $callback[0]();
        }
        return call_user_func($callback);
    }

    public function delete(string $path, array $callback)
    {
        $this->routes['delete'][$path] = $callback;
    }


}